How to RUN:
To START the progrm:
    1. Start MySql si Apache server
    2. Start LibraryApplication
    3. Se acceseaza: http://localhost:8080/repo/add -> Adauga date in db.
    4. Open db on: http://localhost/phpmyadmin
    5. Open Application on: http://localhost:8080/login

To do: Posibile feature-uri:
A. ADMIN
    1. CRUD - pentru autori si carti
        1.1. Adauga autor
        1.2. Adauga carte
        1.3. Afiseaza toti autorii. La click pe un autor se afiseaza toata cartile autorului
                http://localhost:8080/admin/authors
                -> nu are acces la: http://localhost:8080/user/authors, e restrictionat
        1.4. Langa numele autorului si langa numele cartii butoanele:
                - Edit, pentru a edita autorul / cartea
                - Sterge, pentru a sterge autorul / cartea
    2. RUD - pentrunimprumuturi
        2.1. Afiseaza toate imprumuturile si cele aprobate si cele in asteptare. Formatul: user, carte, autor, data
                http://localhost:8080/user/authors
                -> nu are acces la: http://localhost:8080/admin/authors, e restrictionat
        2.2. Langa imprumut butoane:
                - Aproba (daca nu a fost aprobat);
                - Sterge (pt cele aprobate si cele neaprobate)

B. USER
    1. Afiseaza toti autorii. La click pe un autor se afiseaza toata cartile autorului.
    2. Langa numele cartii buton: "Cere imprumut". Se trimite o cerere de imprumut, in asteptare.
    3. Afiseaza toate imprumuturile facute de user. (Aprobate si in asteptare).


Se presupune ca biblioteca are un numar nelimitat de carti, poate imprumta oricarui utilizator cate na de fiecare.