package tpjad.library.model;

import javax.persistence.Column;

/**
 * Created by Alina on 28.01.2018.
 */
public class AuthorMock {
    private int id;
    private String firstName;

    public AuthorMock() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
}
