package tpjad.library.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import tpjad.library.model.Author;
import tpjad.library.model.Loan;
import tpjad.library.model.User;
import tpjad.library.repository.AuthorRepository;
import tpjad.library.repository.BookRepository;
import tpjad.library.repository.LoanRepository;
import tpjad.library.repository.UserRepository;

import tpjad.library.model.Book;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/user/")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private LoanRepository loanRepository; //it will be moved to a separate controller (now we don't have a route for loans.... :( :( :( )

    @GetMapping("/showBooks")
    public ModelAndView showBooks(int id)
    {
        ModelAndView modelAndView = new ModelAndView("user/authorsBooks");

        List<Book> bookList = bookRepository.findAll();
        int currentAuthor = id;

        List<Book> bookListForAuthor = GetListOfBooksForASpecificUserId(bookList, currentAuthor);
        modelAndView.addObject("bookAuthors",bookListForAuthor);

        return  modelAndView;
    }

    @GetMapping("/showImprumuturi")
    public ModelAndView showImprumuturi()
    {
        ModelAndView modelAndView = new ModelAndView("user/userImprumuturi");

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String userName = authentication.getName();
        List<User> userList = userRepository.findAll();
        int idForCurrentUser = GetIdForASpecificUser(userList,userName); //id-ul userului curent
        List<Loan> listLoan = loanRepository.findAll();

        List<Loan> loanForCurrentUser = GetListOfLoanForASpecificUserId(listLoan,idForCurrentUser);

        modelAndView.addObject("imprumuturi",loanForCurrentUser);
        return modelAndView;
    }

    @GetMapping("/imprumutaCarte")
    public @ResponseBody
    String imprumutaCarte(int id)
    {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String userName = authentication.getName();
        List<User> userList = userRepository.findAll();
        int idForCurrentUser = GetIdForASpecificUser(userList,userName); //id-ul userului curent
        int idCurrentBook = id;

        Loan loanToAdd= new Loan(idForCurrentUser, idCurrentBook, 0, Date.valueOf("2018-01-30"));
        loanRepository.save(loanToAdd);

        String ok = "Imprumutul s-a realizat cu succes";
        return ok;
    }

    @GetMapping("/authors")
    public ModelAndView login() {
        ModelAndView modelAndView = new ModelAndView("user/authorsUser");

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String userName = authentication.getName();
        List<User> userList = userRepository.findAll();
        int idForCurrentUser = GetIdForASpecificUser(userList,userName); //id-ul userului curent
        List<Loan> listLoan = loanRepository.findAll();

        List<Loan> loanForCurrentUser = GetListOfLoanForASpecificUserId(listLoan,idForCurrentUser);

        List<Author> listaAutori = authorRepository.findAll();   //findIdByFirstAndLastNameSecond();
        List<Book> listaCarti = bookRepository.GetAllBooksForAnAuthor(1);


        //List<Loan> listaLoansAproved = loanRepository.GetLoanForASpecificUser(1,1);
        //List<Loan> listaLoansPending = loanRepository.GetLoanForASpecificUser(1,0);


        modelAndView.addObject("autori",listaAutori);
        modelAndView.addObject("imprumuturi",loanForCurrentUser);

        return modelAndView;
    }

    private int GetIdForASpecificUser(List<User> userList, String name)
    {
        int id = 0;
        for(int i = 0; i < userList.size(); ++i)
        {
            String userForCurrentLoan = userList.get(i).getUsername();
            if(userForCurrentLoan.compareTo(name) == 0)
            {
                id = userList.get(i).getId();
            }
        }

        return id;
    }

    private  List<Loan> GetListOfLoanForASpecificUserId(List<Loan> loanList, int userId)
    {
        List<Loan> newLoanList = new ArrayList<Loan>();

        for(int i = 0; i < loanList.size(); ++i)
        {
            if(loanList.get(i).getIdUser() == userId)
            {
                newLoanList.add(loanList.get(i));
            }
        }

        return newLoanList;
    }

    private List<Book> GetListOfBooksForASpecificUserId(List<Book> bookList, int userId)
    {
        List<Book> newBookList = new ArrayList<Book>();

        for(int i = 0; i < bookList.size(); ++i)
        {
            if(bookList.get(i).getIdAuthor() == userId)
            {
                newBookList.add(bookList.get(i));
            }
        }

        return newBookList;
    }



}
