package tpjad.library.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import tpjad.library.model.Book;

import javax.persistence.Column;
import java.util.List;

public interface BookRepository extends JpaRepository<Book, Long> {

    @Query("SELECT id from Book where title = :title and idAuthor = :idAuthor")
    List<Integer> findByTitleAndIdAuthor(@Param("title") String title, @Param("idAuthor") int idAuthor);


    @Query("SELECT id,title,isbn,publisher from Book where idAuthor = :idAuthor")
    List<Book> GetAllBooksForAnAuthor(@Param("idAuthor") int idAuthor);


}
