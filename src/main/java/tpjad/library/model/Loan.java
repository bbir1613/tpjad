package tpjad.library.model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

@Entity
@Table(name = "loan")
public class Loan implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_loan")
    private int id;

    @Column(name = "id_user")
    private int idUser;
    @Column(name = "id_book")
    private int idBook;

    /**
     * approved = 1 => true ( is approved )
     * approved = 0 => false ( is not approved )
     */
    @Column(name = "approved")
    private int approved;

    @Column(name = "date")
    private Date date;

    public Loan(){}

    public Loan(int idUser, int idBook, int approved, Date date) {
        this.idUser = idUser;
        this.idBook = idBook;
        this.approved = approved;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public int getIdBook() {
        return idBook;
    }

    public void setIdBook(int idBook) {
        this.idBook = idBook;
    }

    public int getApproved() {
        return approved;
    }

    public void setApproved(int approved) {
        this.approved = approved;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int isApproved() {
        return approved;
    }

}