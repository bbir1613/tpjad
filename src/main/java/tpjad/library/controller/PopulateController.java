package tpjad.library.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import tpjad.library.model.Author;
import tpjad.library.model.Book;
import tpjad.library.model.Loan;
import tpjad.library.repository.AuthorRepository;
import tpjad.library.repository.BookRepository;
import tpjad.library.repository.LoanRepository;
import tpjad.library.repository.UserRepository;
import tpjad.library.model.User;

import java.sql.Date;

@Controller
@RequestMapping("/repo")
public class PopulateController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private LoanRepository loanRepository;

    @GetMapping("/add")
    public @ResponseBody
    String addData() {
        String retunredString = "";
        retunredString += addUsers();
        retunredString += addAuthors();
        retunredString += addBooks();
        retunredString += addLoans();
        return retunredString;
    }

    private String addLoans() {
        int idUser = userRepository.findIdByUsername("a").get(0);
        int idAuthor = authorRepository.findIdByFirstAndLastName("Magda", "Isanos").get(0);
        int idBook = bookRepository.findByTitleAndIdAuthor("Poezii", idAuthor).get(0);
        Loan loan_1 = new Loan(idUser, idBook, 0, Date.valueOf("2018-01-22"));
        loanRepository.save(loan_1);

        idBook = bookRepository.findByTitleAndIdAuthor("Cântarea munţilor", idAuthor).get(0);
        Loan loan_2 = new Loan(idUser, idBook, 1, Date.valueOf("2018-01-22"));
        loanRepository.save(loan_2);

        idAuthor = authorRepository.findIdByFirstAndLastName("Daniel", "Turcea").get(0);
        idBook = bookRepository.findByTitleAndIdAuthor("Urme in vesnicie", idAuthor).get(0);
        Loan loan_3 = new Loan(idUser, idBook, 1, Date.valueOf("2018-01-22"));
        loanRepository.save(loan_3);

        Loan loan_4= new Loan(2, 6, 1, Date.valueOf("2018-01-22"));
        loanRepository.save(loan_4);

        return " Loans added. \n";
    }

    private String addBooks() {
        int idAuthor_1 = authorRepository.findIdByFirstAndLastName("Daniel", "Turcea").get(0);
        Book book_1 = new Book("Urme in vesnicie", "978-606-666-002-0", idAuthor_1, "Doxologia", 2013);
        bookRepository.save(book_1);

        Book book_2 = new Book("Epifania", "978-606-8117-98-0", idAuthor_1, "Doxologia", 2011);
        bookRepository.save(book_2);

        Book book_3 = new Book("Iubire, intlelepciune fara sfirsit", "978-9732402115", idAuthor_1, "Albatros", 1991);
        bookRepository.save(book_3);

        int idAuthor_2 = authorRepository.findIdByFirstAndLastName("Mihai", "Eminescu").get(0);
        Book book_4 = new Book("Poezii", "978-973-128-453-8", idAuthor_2, "Corint Junior", 2014);
        bookRepository.save(book_4);

        int idAuthor_3 = authorRepository.findIdByFirstAndLastName("Magda", "Isanos").get(0);
        Book book_5 = new Book("Poezii", "973-9016-42-1", idAuthor_3, "Libra", 1996);
        bookRepository.save(book_5);

        Book book_6 = new Book("Cântarea munţilor", " - ", idAuthor_3, "Minerva", 1988);
        bookRepository.save(book_6);

        return " Books added. \n";
    }

    private String addAuthors() {
        Author author_1 = new Author();
        author_1.setFirstName("Mihai");
        author_1.setLastName("Eminescu");
        author_1.setBirthday("15.01.1850");
        authorRepository.save(author_1);

        Author author_2 = new Author();
        author_2.setFirstName("Daniel");
        author_2.setLastName("Turcea");
        author_2.setBirthday("22.07.1945");
        authorRepository.save(author_2);

        Author author_3 = new Author();
        author_3.setFirstName("Magda");
        author_3.setLastName("Isanos");
        author_3.setBirthday("17.04.1916");
        authorRepository.save(author_3);
        return " Authors added. \n";
    }

    private String addUsers() {
        User admin_user = new User();
        admin_user.setUsername("a");
        admin_user.setPassword("a");
        admin_user.setRole("ADMIN");
        userRepository.save(admin_user);

        User normal_user = new User();
        normal_user.setUsername("b");
        normal_user.setPassword("a");
        normal_user.setRole("USER");
        userRepository.save(normal_user);
        return " Users added. \n";
    }
}
