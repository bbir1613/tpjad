package tpjad.library.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import tpjad.library.model.Author;
import tpjad.library.model.Book;
import tpjad.library.model.Loan;
import tpjad.library.repository.AuthorRepository;
import tpjad.library.repository.BookRepository;
import tpjad.library.repository.LoanRepository;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/admin/")
public class AdminController {

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private LoanRepository loanRepository; //it will be moved to a separate controller (now we don't have a route for loans.... :( :( :( )


    @GetMapping("/authors")
    public ModelAndView login() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("admin/authorsAdmin");

        List<Author> listaAutori = authorRepository.findAll();   //findIdByFirstAndLastNameSecond();
        List<Loan> loans = loanRepository.findAll();
       // List<Book> allBooksForAnAuthor = bookRepository.GetAllBooksForAnAuthor();
        int id = 1;

        //----------delete loans-----------
        //Loan loanToDelete = getSpecificLoan(id,loans);
        //loanRepository.delete(loanToDelete);

        //---------update loans-----------
        //Loan loanToUpdate = getSpecificLoan(id,loans);
        //loanToUpdate.setApproved(1);
        //loanRepository.save(loanToUpdate);

        modelAndView.addObject("autori",listaAutori);
        modelAndView.addObject("imprumuturi1",loans);

        return modelAndView;
    }

    @GetMapping("/acceptaImprumut")
    public ModelAndView acceptaImprumut(int id)
    {
        ModelAndView modelAndView = new ModelAndView("admin/adminAcceptaImprumut");
        List<Loan> loans = loanRepository.findAll();

        //---------update loans-----------
        Loan loanToUpdate = getSpecificLoan(id,loans);
        loanToUpdate.setApproved(1);
        loanRepository.save(loanToUpdate);

        return  modelAndView;
    }

    @GetMapping("/stergeImprumut")
    public ModelAndView stergeImprumut(int id)
    {
        ModelAndView modelAndView = new ModelAndView("admin/adminStergeImprumut");
        List<Loan> loans = loanRepository.findAll();

        //----------delete loans-----------
        Loan loanToDelete = getSpecificLoan(id,loans);
        loanRepository.delete(loanToDelete);

        return  modelAndView;
    }

    @GetMapping("/showImprumuturi")
    public ModelAndView showImprumuturi()
    {
        ModelAndView modelAndView = new ModelAndView("admin/adminImprumuturi");
        List<Loan> loans = loanRepository.findAll();
        modelAndView.addObject("imprumuturi",loans);

        return modelAndView;
    }

    @GetMapping("/showBooks")
    public ModelAndView showBooks(int id)
    {
        ModelAndView modelAndView = new ModelAndView("admin/authorsBooks");

        List<Book> bookList = bookRepository.findAll();
        int currentAuthor = id;

        List<Book> bookListForAuthor = GetListOfBooksForASpecificUserId(bookList, currentAuthor);
        modelAndView.addObject("bookAuthors",bookListForAuthor);

        return  modelAndView;
    }


    private List<Book> GetListOfBooksForASpecificUserId(List<Book> bookList, int userId)
    {
        List<Book> newBookList = new ArrayList<Book>();

        for(int i = 0; i < bookList.size(); ++i)
        {
            if(bookList.get(i).getIdAuthor() == userId)
            {
                newBookList.add(bookList.get(i));
            }
        }

        return newBookList;
    }

    private Loan getSpecificLoan(int id, List<Loan> loans)
    {
        Loan specificLoan = null;
        for(int i = 0; i < loans.size(); ++i)
        {
            Loan l = loans.get(i);
            if(l.getId() == id)
            {
                specificLoan = loans.get(i);
            }
        }

        return specificLoan;
    }

    @GetMapping("/stergeAuthor")
    public ModelAndView stergeAuthor(int id)
    {
        ModelAndView modelAndView = new ModelAndView("admin/adminStergeAuthor");
        List<Author> authorList = authorRepository.findAll();

        //----------delete author-----------
        Author authorToDelete = getSpecificAuthor(id,authorList);
        authorRepository.delete(authorToDelete);

        return  modelAndView;
    }

    private Author getSpecificAuthor(int id, List<Author> authors)
    {
        Author specificAuthor = null;
        for(int i = 0; i < authors.size(); ++i)
        {
            Author a = authors.get(i);
            if(a.getId() == id)
            {
                specificAuthor = authors.get(i);
            }
        }

        return specificAuthor;
    }

    @GetMapping("/editAuthor")
    public ModelAndView editAuthor(int id)
    {
        ModelAndView modelAndView = new ModelAndView("admin/adminEditAuthor");
        List<Author> authorList = authorRepository.findAll();

        //----------edit author-----------
        Author authorToEdit = getSpecificAuthor(id,authorList);

        modelAndView.addObject("autor",authorToEdit);
        return  modelAndView;
    }

    // Nu face update - eroare
    @PostMapping ("/updateAuthor")
    public ModelAndView updateAuthor(@RequestParam("id") int id, @RequestParam("firstName") String firstName, @RequestParam("lastName") String lastName, @RequestParam("birthday") String birthday)
    {
        ModelAndView modelAndView = new ModelAndView("admin/Succesfull");

        List<Author> authors = authorRepository.findAll();

        //---------update authors-----------
        Author authorToUpdate = getSpecificAuthor(id,authors);
        authorToUpdate.setFirstName(firstName);
        authorToUpdate.setLastName(lastName);
        authorToUpdate.setBirthday(birthday);
        authorRepository.save(authorToUpdate);

        return  modelAndView;
    }

    @GetMapping("/addAuthor")
    public ModelAndView addAuthor() {
        return new ModelAndView("admin/adminAddAuthor");
    }

    @PostMapping("/saveAuthor")
    public ModelAndView saveAuthor(@RequestParam("firstName") String firstName, @RequestParam("lastName") String lastName, @RequestParam("birthday") String birthday){
        ModelAndView modelAndView = new ModelAndView("admin/Succesfull");
        Author author = new Author();
        author.setFirstName(firstName);
        author.setLastName(lastName);
        author.setBirthday(birthday);
        authorRepository.save(author);
        return modelAndView;
    }

    @GetMapping("/addBook")
    public ModelAndView addBook() {
        return new ModelAndView("admin/adminAddBook");
    }

    @PostMapping("/saveBook")
    public ModelAndView saveBook(@RequestParam("title") String title, @RequestParam("isbn") String isbn, @RequestParam("idAuthor") int idAuthor, @RequestParam("publisher") String publisher, @RequestParam("publishedYead") int publishedYead){
        ModelAndView modelAndView = new ModelAndView("admin/Succesfull");
        Book book = new Book(title,isbn, idAuthor, publisher, publishedYead);
        bookRepository.save(book);
        return modelAndView;
    }

}
