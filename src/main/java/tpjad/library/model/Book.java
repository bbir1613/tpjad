package tpjad.library.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "book")
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_book")
    private int id;
    @Column(name = "title")
    private String title;
    @Column(name = "isbn")
    private String isbn;
    @Column(name = "id_author")
    private int idAuthor;
    @Column(name = "publisher")
    private String publisher;
    @Column(name = "publishedYead")
    private int publishedYead;

    public Book(String title, String isbn, int idAuthor, String publisher, int publishedYead) {
        this.title = title;
        this.isbn = isbn;
        this.idAuthor = idAuthor;
        this.publisher = publisher;
        this.publishedYead = publishedYead;
    }

    public Book(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public int getIdAuthor() {
        return idAuthor;
    }

    public void setIdAuthor(int idAuthor) {
        this.idAuthor = idAuthor;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public int getPublishedYead() {
        return publishedYead;
    }

    public void setPublishedYead(int publishedYead) {
        this.publishedYead = publishedYead;
    }
}