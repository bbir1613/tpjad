package tpjad.library.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import tpjad.library.model.Author;
import tpjad.library.model.AuthorMock;

import java.util.List;

public interface AuthorRepository extends JpaRepository<Author, Long> {

    @Query("SELECT id from Author where firstName = :firstName and lastName = :lastName")
    List<Integer> findIdByFirstAndLastName(@Param("firstName") String firstName, @Param("lastName") String lastName);

    @Query("SELECT id,firstName from Author where firstName = :firstName")
    List<AuthorMock> findIdByFirstName(@Param("firstName") String firstName);

    @Query("SELECT id,firstName,lastName,birthday from Author")
    List<Author> findIdByFirstAndLastNameSecond();
}
