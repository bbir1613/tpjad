package tpjad.library.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import tpjad.library.model.User;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long>{
	User findByUsername(String username);
	
	@Query("SELECT role from User where id = :id")
	String findRoleByUserId(@Param("id")int id);

	@Query("SELECT id from User where username = :username")
	List<Integer> findIdByUsername(@Param("username") String username);
}
