package tpjad.library.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import tpjad.library.model.Loan;

import java.util.List;

public interface LoanRepository extends JpaRepository<Loan, Long> {


    @Query("SELECT id,idUser,idBook,approved,date from Loan where idUser = :idUser and approved = :approved")
    List<Loan> GetLoanForASpecificUser(@Param("idUser") int idUser, @Param("approved") int approved);

    @Query("SELECT id,idUser,idBook,approved,date from Loan where id = :id")
    Loan GetASpecificLoan(@Param("id") int id);

    @Query("SELECT id,idUser,idBook,approved,date from Loan")
    List<Loan> GetAllLoans();

}
